#!/bin/bash
files=`find $1 -name "*.jpg" -printf "%T@ %p\n" | sort -n | cut -f2 -d' '`
convert -append $files ${1////_}.jpg

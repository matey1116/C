#!/bin/bash

ALPHABET=$(sed -n '2p' $1)
alphabet=$(sed -n '1p' $1)

cat $2 | tr 'A-Z' "$ALPHABET" | tr 'a-z' "$alphabet"
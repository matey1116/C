#!/bin/bash
ALPHABET=$(sed -n '2p' $1)
alphabet=$(sed -n '1p' $1)
cat $2 | tr "$ALPHABET" 'A-Z' | tr "$alphabet" 'a-z'

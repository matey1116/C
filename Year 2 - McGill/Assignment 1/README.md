# Assignment 1

## Question 2

Bash program that encrypts and decrypts string data with a given "Codebook"

## Question 3

Bash program that creates a timeline that summarizes all photos in a give directory (and recursive sub-directories) of the first command-line argument

### Prerequisites

imagemagick

## Authors

* **Matey Marinov** - [matey1116](https://github.com/matey1116)

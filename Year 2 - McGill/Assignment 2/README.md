# Assignment 2

## Question 1
### Part A

A program which takes 1 argument, the height H of the diamond. Prints a diamond which is made up of H rows of asterisk (a.k.a. star, *) characters and spaces

### Part B

A program which prints a modified diamond, such that each of the top and bottom half are Sierpiński Triangles and takes two parameters:  the height of the diamond, H, and the fractal level, L

## Question 2

A program that parses pages from the site Wikipedia using C’s text processing functions found in <string.h>

## Authors

* **Matey Marinov** - [matey1116](https://github.com/matey1116)

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

void print_row(int row,int height,int level){
  // base case for recursion which prints a triangle row by row
  if (level == 1){
    int stars = (2*(row-1)) + 1;
    int space = (height-stars)/2;
    for (int j = 0; j < space; j++){
      printf(" ");
    }
    for (int k = 0; k < stars; k++){
      printf("*");
    }
    for (int j = 0; j < space; j++){
      printf(" ");
    }
  }
  //top half of the triangle to be printed
  else if (row < ((height+1)/4) + 1){
    int space = ((height + 1)/4);
    for (int j = 0; j < space; j++){
      printf(" ");
    }
    print_row(row,height/2,level-1);
    for (int j = 0; j < space; j++){
      printf(" ");
    }
  }
  //bottom half of the triangle to be printed
  else{
    print_row(row - ((height+1)/4) , ((height-1)/2), level - 1);
    printf(" ");
    print_row(row - ((height+1)/4) , ((height-1)/2), level - 1);
  }
}

bool IsPowerOfTwo(int x)
{
    return (x != 0) && ((x & (x - 1)) == 0);
}

int main (int argc, char** argv){
  if(argc < 3 || argc > 3){
    printf("Provide exactly 2 arguments\n");
    return 1;
  }
  char *h = argv[1];
  char *l = argv[2];
  if(atoi(h) % 2 == 0 || atoi(h) < 1) {
    printf("ERROR: Bad argument. Height must be positive odd integer \n");
    return 1;
  }
  else if(atoi(l) < 1){
    printf("ERROR: Bad argument. Level must be positive integer \n");
    return 1;
  }
  else{
    int tri_height = ceil(atoi(h)/2.0);
    if(IsPowerOfTwo(tri_height) && tri_height >= pow(2, atoi(l)-1) && atoi(l) > 1){
      for(int row = 0; row <= (atoi(h)/2) + 1; row++){
        print_row(row, atoi(h), atoi(l));
        printf("\n");
      }
      for(int row = atoi(h)/2; row > 0; row--){
        print_row(row, atoi(h), atoi(l));
        printf("\n");
      }
    }
    else if (atoi(l) == 1){
      for(int row = 0; row <= (atoi(h)/2) + 1; row++){
        print_row(row, atoi(h), atoi(l));
        printf("\n");
      }
      for(int row = atoi(h)/2; row > 0; row--){
        print_row(row, atoi(h), atoi(l));
        printf("\n");
      }
    }
    else {
      printf("ERROR: Height does not allow evenly dividing requested number of levels.\n");
      return 1;
    }
  }
  return 0;
}


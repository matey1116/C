#include <stdio.h>
#include<stdlib.h>
 
void diamond(int number)
{
  int n, c, k, space = 1;
  n = number;
  space = n/2 ;
  for (k = 1; k <= n/2 + 1; k++)
  {
    for (c = 1; c <= space; c++)
      printf(" ");
    space--;

    for (c = 1; c < 2*k; c++)
      printf("*");
    printf("\n");
  }
 
  space = 1;
 
  for (k = n/2; k >= 1; k--)
  {
    for (c = 1; c <= space; c++)
      printf(" ");
 
    space++;
 
    for (c = 1 ; c <= 2*k-1; c++)
      printf("*");
 
    printf("\n");
  }
}

int main(int argc, char** argv){
  if(argc < 2 || argc > 2){
    printf("ERROR: Wrong number of arguments. One required. \n");
    return 1;
  }
  char *number = argv[1];
  if(atoi(number) % 2 == 0 || atoi(number) < 1) {
    printf("ERROR: Bad argument. Height must be positive odd integer \n");
    return 1;
  }
  else{
    diamond(atoi(number));
  }
  return 0;
}


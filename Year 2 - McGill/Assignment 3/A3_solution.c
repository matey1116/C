/* FILE: A3_solutions.c is where you will code your answers for Assignment 3.
 *
 * Each of the functions below can be considered a start for you. They have
 * the correct specification and are set up correctly with the header file to
 * be run by the tester programs.
 *
 * You should leave all of the code as is, especially making sure not to change
 * any return types, function name, or argument lists, as this will break
 * the automated testing.
 *
 * Your code should only go within the sections surrounded by
 * comments like "REPLACE EVERTHING FROM HERE... TO HERE.
 *
 * The assignment document and the header A3_solutions.h should help
 * to find out how to complete and test the functions. Good luck!
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "A3_provided_functions.h"

unsigned char*
bmp_open( char* bmp_filename,        unsigned int *width,
          unsigned int *height,      unsigned int *bits_per_pixel,
          unsigned int *padding,     unsigned int *data_size,
          unsigned int *data_offset                                  )
{
  unsigned char *img_data=NULL;

  FILE *bmpfile = fopen( bmp_filename, "rb");

  if( bmpfile == NULL ){
  	  printf( "I was unable to open the bmp file .\n" );
  	  return NULL;
  	}

  // Read the overall file size
  fseek (bmpfile,0,SEEK_END);
  *data_size = ftell(bmpfile);
  rewind(bmpfile);

  img_data = (unsigned char *)malloc((*data_size+1) * sizeof(char));
  fread(img_data,*data_size,1,bmpfile);

  *data_offset = *(unsigned int *)&img_data[10];
  *width = *(unsigned int *)&img_data[18];
  *height = *(unsigned int *)&img_data[22];
  short int bpp = *(int *)&img_data[28];
  *bits_per_pixel = bpp;
  *padding = *width % 4;

  return img_data;
}

void
bmp_close( unsigned char **img_data )
{
  free(*img_data);
  img_data = NULL;
}
unsigned char***
bmp_scale( unsigned char*** pixel_array, unsigned char* header_data, unsigned int header_size,
           unsigned int* width, unsigned int* height, unsigned int num_colors,
           float scale )
{

  int new_height = (unsigned int) (*height * scale);
  int new_width = (unsigned int) (*width * scale);

  unsigned char*** new_pixel_array = (unsigned char***)malloc( sizeof(unsigned char**) * (new_height));
  for( int row=0; row< new_height; row++ ){
    new_pixel_array[row] = (unsigned char**)malloc( sizeof(unsigned char*) * (new_width) );
    for( int col=0; col<new_width; col++ ){
      new_pixel_array[row][col] = (unsigned char*)malloc( sizeof(unsigned char) * (num_colors) );
    }
  }

  for (int row = 0; row < new_height; row++){
    for (int col = 0; col < new_width; col++){
      new_pixel_array[row][col] = pixel_array[(int)(row / scale)][(int)(col / scale)];
    }
  }

  *height = new_height;
  *width =  new_width;

  unsigned u, u1, u2, u3;
  for (int i = 0; i < sizeof(int); i++){
    u = new_width;
    u = u >> (8 * i);
    u1 = u & 0xff;
    header_data[18+i] = (unsigned char)u1;

    u2 = new_height;
    u2 = u2 >> (8 * i);
    u3 = u2 & 0xff;
    header_data[22+i] = (unsigned char)u3;

   }
  return new_pixel_array;
}
int
bmp_collage( char* background_image_filename,     char* foreground_image_filename,
             char* output_collage_image_filename, int row_offset,

             int col_offset,                      float scale )
{

  unsigned char*   header_data_fg;
  unsigned int     header_size_fg, width_fg, height_fg, num_colors_fg;
  unsigned char*** pixel_array_fg = NULL;

  pixel_array_fg = bmp_to_3D_array( foreground_image_filename, &header_data_fg, &header_size_fg,  &width_fg, &height_fg, &num_colors_fg);
  
  unsigned char*** scaled_pixel_array_fg = bmp_scale( pixel_array_fg, header_data_fg, header_size_fg, &width_fg, &height_fg, num_colors_fg, scale );

  unsigned char*   header_data_bg;
  unsigned int     header_size_bg, width_bg, height_bg, num_colors_bg;
  unsigned char*** pixel_array_bg = NULL;

  pixel_array_bg = bmp_to_3D_array( background_image_filename, &header_data_bg, &header_size_bg,  &width_bg, &height_bg, &num_colors_bg);

  if (pixel_array_bg == NULL || pixel_array_fg == NULL){
     printf("Error opening images\n");
     return -1;
  }
  
  if (num_colors_fg != 4 || num_colors_bg != 4){
     printf("One of the images has wrong number of colors");
     return -1;
  }
  
  if (width_fg > width_bg || height_fg > height_bg){
     printf("Scaled foreground image is bigger that the background image");
     return -1; 
  } 

  for( int row=0 ; row< height_fg; row++ ){
    for( int col=0; col<width_fg; col++ ){
      if (scaled_pixel_array_fg[row][col][0] != 0){
        pixel_array_bg[row + row_offset][col + col_offset] = scaled_pixel_array_fg[row][col];
      }
    }
  }


  bmp_from_3D_array( output_collage_image_filename, header_data_bg, header_size_bg,pixel_array_bg, width_bg, height_bg, num_colors_bg);

  return 0;
}

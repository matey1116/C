#include <stdio.h>      //printf
#include <string.h>     //strlen
#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

void str_replace(char *target, const char *needle, const char *replacement)
{
  char buffer[1024] = {0};
  char *insert_point = &buffer[0];
  const char *tmp = target;
  size_t needle_len = strlen(needle);
  size_t repl_len = strlen(replacement);

  while (1)
  {
    const char *p = strstr(tmp, needle);

    if (p == NULL)
    {
      strcpy(insert_point, tmp);
      break;
    }

    memcpy(insert_point, tmp, p - tmp);
    insert_point += p - tmp;

    memcpy(insert_point, replacement, repl_len);
    insert_point += repl_len;

    tmp = p + needle_len;
  }

  strcpy(target, buffer);
}

int isValidIpAddress(char *ipAddress)
{
  struct sockaddr_in sa;
  int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
  if (result == 1)
    return 1;
  return 0;
}

int main(void)
{
  printf("%s%c%c\n", "Content-Type:text/html;charset=iso-8859-1", 13, 10);
  char *data;
  char ip[20];
  char port1[100];
  char username[100];
  char password[100];
  char gamename[100];
  char square[100];
  char *command;
  //int port

  data = getenv("QUERY_STRING");
  sscanf(data, "%*[^=]%*c%[^&]%*[^=]%*c%[^&]%*[^=]%*c%[^&]%*[^=]%*c%[^&]%*[^=]%*c%[^&]%*[^=]%*c%[^&]%*[^=]%*c%s", ip, port1, username, password, gamename, square, command);

  command = strrchr(data, '=');
  command++;

  unsigned short int port = (unsigned short)atoi(port1);

  if (port == 0)
  {
    printf("ERROR FROM PORT<br/>");
    return 0;
  }

  if (isValidIpAddress(ip) == 0)
  {
    printf("NOT VALID IP<br/>");
    return 0;
  }

  int sock;
  struct sockaddr_in server;

  server.sin_addr.s_addr = inet_addr(ip);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);

  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1)
  {
    printf("Could not create socket<br/>");
    return 0;
  }

  int connection = connect(sock, (struct sockaddr *)&server, sizeof(server));

  //Connect to remote server
  if (connection < 0)
  {
    printf("connect failed. Error");
    return 0;
  }

  char message[200];
  strcpy(message, command);
  strcat(message, ",");
  strcat(message, username);
  strcat(message, ",");
  strcat(message, password);
  strcat(message, ",");
  strcat(message, gamename);
  strcat(message, ",");
  strcat(message, square);
  message[strlen(message) + 1] = '\0';

  if (send(sock, message, 200, 0) < 0)
  {
    puts("Send failed");
    return 1;
  }

  size_t read_size;
  int bytes_read = 0;
  char incoming_msg[2000];
  char temp[2000];
  while (bytes_read < 2000)
  {
    read_size = recv(sock, temp, 2000, 0);
    if (read_size <= 0)
    {
      puts("Server disconnected");
      fflush(stdout);
      close(sock);
      return 0;
    }
    memcpy(incoming_msg + bytes_read, temp, read_size);
    bytes_read += read_size;
  }

  str_replace(incoming_msg, "\n", "<br>");
  str_replace(incoming_msg, " ", "&ensp;");
  str_replace(incoming_msg, "-", "&ndash;");

  printf("<form action=\"ttt.cgi\">");
  printf("<b>Server Address: <input type=\"text\" name=\"address\" size=\"20\" value=\"%s\"><br />", ip);
  printf("<b>Server Port: <input type=\"text\" name=\"port\" size=\"20\" value=\"%d\"><br />", port);
  printf("<b>Username: <input type=\"text\" name=\"username\" size=\"20\"><br />");
  printf("<b>Password: <input type=\"text\" name=\"password\" size=\"20\"><br />");
  printf("<b>Gamename: <input type=\"text\" name=\"gamename\" size=\"20\"><br />");
  printf("<b>Square: <input type=\"text\" name=\"square\" size=\"20\"><br />");
  printf("<input type=\"submit\" value=\"LOGIN\" name=\"LOGIN\">");
  printf("<input type=\"submit\" value=\"CREATE\" name=\"CREATE\">");
  printf("<input type=\"submit\" value=\"JOIN\" name=\"JOIN\">");
  printf("<input type=\"submit\" value=\"MOVE\" name=\"MOVE\">");
  printf("<input type=\"submit\" value=\"LIST\" name=\"LIST\">");
  printf("<input type=\"submit\" value=\"SHOW\" name=\"SHOW\">");
  printf("</form><br />");

  printf(incoming_msg);

  close(sock);

  return 0;
}

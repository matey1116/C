/*********************************************************
* FILE: ttt_server.c
* 
* A starter code for your answer to A4 Q1. Most of the
* networking is already done for you, and we even gave
* suggested data types (structs and linked lists of these)
* to represent users and tic-tac-toe games. You must just
* figure out how to fill in the various functions required
* to make the games happen.
*
* Good luck, and rember to ask questions quickly if you get 
* stuck. My Courses Dicussions is the first place to try, 
* then office hours.
*
* AUTHOR: YOU!
* DATE: Before Dec 3rd
***********************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

struct USER
{
	char username[100];
	char password[100];
	struct USER *next;
};

enum GAME_STATE
{
	CREATOR_WON = -2,
	IN_PROGRESS_CREATOR_NEXT = -1,
	DRAW = 0,
	IN_PROGRESS_CHALLENGER_NEXT = 1,
	CHALLENGER_WON = 2
};

struct GAME
{
	char gamename[100];
	struct USER *creator;
	struct USER *challenger;
	enum GAME_STATE state;
	char ttt[3][3];
	struct GAME *next;
};

struct USER *user_list_head = NULL;
struct GAME *game_list_head = NULL;

int check_draw(char board[3][3])
{
	for (int row = 0; row < 3; row++)
	{
		for (int column = 0; column < 3; column++)
		{
			if (board[row][column] == ' ')
			{
				return 0;
			}
		}
	}
	return 1;
}

int check_winner(char board[3][3])
{
	int row, col, count;
	// Check each of 3 rows:
	for (row = 0; row < 3; ++row)
	{
		count = 0;
		for (col = 0; col < 3; ++col)
		{
			count += (board[row][col] == 'x') ? 1 : (board[row][col] == 'o') ? -1 : 0;
		}
		if (count == 3 || count == -3)
		{
			return count / abs(count); // Return either 1 or -1
		}
	}

	// Check each of 3 columns.
	for (col = 0; col < 3; ++col)
	{
		count = 0;
		for (row = 0; row < 3; ++row)
		{
			count += (board[row][col] == 'x') ? 1 : (board[row][col] == 'o') ? -1 : 0;
		}
		if (count == 3 || count == -3)
		{
			return count / abs(count); // Return either 1 or -1
		}
	}

	if (board[0][0] == 'x' && board[1][1] == 'x' && board[2][2] == 'x')
	{
		return 1;
	}
	else if (board[0][0] == 'o' && board[1][1] == 'o' && board[2][2] == 'o')
	{
		return -1;
	}

	else if (board[2][0] == 'x' && board[1][1] == 'x' && board[0][2] == 'x')
	{
		return 1;
	}
	else if (board[2][0] == 'o' && board[1][1] == 'o' && board[0][2] == 'o')
	{
		return -1;
	}

	return 0; //nothing
}

int place_char(struct GAME *find_game, char *position, char c)
{

	char row = position[0];
	int column = (int)position[1] - 48;

	if (!(row >= 'a' && row <= 'c'))
	{
		return 10;
	}
	else
	{
		if (!(column >= 1 && column <= 3))
		{
			return 11;
		}
		else
		{
			switch (row)
			{
			case 'a':
				if (find_game->ttt[0][column - 1] != ' ')
					return -1;
				find_game->ttt[0][column - 1] = c;
				break;
			case 'b':
				if (find_game->ttt[1][column - 1] != ' ')
					return -1;
				find_game->ttt[1][column - 1] = c;
				break;
			case 'c':
				if (find_game->ttt[2][column - 1] != ' ')
					return -1;
				find_game->ttt[2][column - 1] = c;
				break;
			default:
				break;
			}
		}
	}

	return 1;
}

int auth(char *username, char *password)
{
	if (user_list_head == NULL)
	{
		return -1;
	}
	else
	{
		struct USER *local = user_list_head;
		while (local != NULL)
		{
			if (strcmp(local->username, username) == 0)
			{
				if (strcmp(local->password, password) == 0)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			local = local->next;
		}
		return -1;
	}
}

int main(int argc, char *argv[])
{
	int socket_desc, client_sock, c, read_size;
	struct sockaddr_in server, client;
	char client_message[2000];

	unsigned short int port = 1998;

	if (argc > 1)
		port = (unsigned short int)atoi(argv[1]);

	//Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);

	if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		perror("bind failed. Error");
		return 1;
	}

	listen(socket_desc, 3);

	printf("Game server ready on port %d.\n", port);

	while (1)
	{
		c = sizeof(struct sockaddr_in);

		//accept connection from an incoming client
		client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c);
		if (client_sock < 0)
		{
			perror("accept failed");
			return 1;
		}

		char temp[200];
		memset(client_message, '\0', 200);
		int bytes_read = 0;
		while (bytes_read < 200)
		{
			read_size = recv(client_sock, temp, 200, 0);
			if (read_size <= 0)
			{
				puts("Client disconnected");
				fflush(stdout);
				close(client_sock);
				close(socket_desc);
				return 0;
			}
			memcpy(client_message + bytes_read, temp, read_size);
			bytes_read += read_size;
		}

		char response[2000];
		response[0] = '\0';
		char *command = strtok(client_message, ",");
		char *username = strtok(NULL, ",");
		char *password = strtok(NULL, ",");

		if (command == NULL || username == NULL || password == NULL)
		{
			sprintf(response, "MUST ENTER A VALID COMMAND WITH ARGUMENTS FROM THE LIST:\n");
			sprintf(response + strlen(response), "LOGIN,USER,PASS\n");
			sprintf(response + strlen(response), "CREATE,USER,PASS,GAMENAME\n");
			sprintf(response + strlen(response), "JOIN,USER,PASS,GAMENAME,SQUARE\n");
			sprintf(response + strlen(response), "MOVE,USER,PASS,GAMENAME,SQUARE\n");
			sprintf(response + strlen(response), "LIST,USER,PASS\n");
			sprintf(response + strlen(response), "SHOW,USER,PASS,GAMENAME\n");
			write(client_sock, response, 2000);
			close(client_sock);
			continue;
		}

		if (strcmp(command, "LOGIN") == 0)
		{
			//check if list is empty
			int exists = 0;
			int logged = auth(username, password);
			if (logged == 1)
			{
				strcpy(response, "EXISTING USER LOGIN OK");
				exists = 1;
			}
			else if (logged == 0)
			{
				strcpy(response, "BAD PASSWORD");
				exists = 1;
			}
			if (exists == 0)
			{
				struct USER *new_user = (struct USER *)malloc(sizeof(struct USER));
				strcpy(new_user->username, username);
				strcpy(new_user->password, password);
				new_user->next = NULL;
				if (user_list_head == NULL)
				{
					user_list_head = new_user;
					strcpy(response, "NEW USER CREATED OK");
				}
				else
				{
					struct USER *last = user_list_head;
					while (last->next != NULL)
					{
						last = last->next;
					}
					last->next = new_user;
					strcpy(response, "NEW USER CREATED OK");
				}
			}
		}
		else if (strcmp(command, "CREATE") == 0)
		{
			int logged = auth(username, password);
			if (logged == 1)
			{
				char *game_name = strtok(NULL, ",");

				if (game_name == NULL)
				{
					sprintf(response, "CREATE COMMAND MUST BE CALLED AS FOLLOWS:\n");
					sprintf(response + strlen(response), "CREATE,USER,PASS,GAMENAME\n");
					write(client_sock, response, 2000);
					close(client_sock);
					continue;
				}

				struct GAME *find_game = game_list_head;
				while (find_game != NULL)
				{
					if (strcmp(find_game->gamename, game_name) == 0)
					{
						sprintf(response, "GAME WITH %s NAME ALREADY EXISTS", game_name);
						goto end;
					}
					find_game = find_game->next;
				}

				struct GAME *game = (struct GAME *)malloc(sizeof(struct GAME));
				strcpy(game->gamename, game_name);
				for (int row = 0; row < 3; row++)
					for (int col = 0; col < 3; col++)
						game->ttt[row][col] = ' ';

				//find the user to link it to the game
				struct USER *find_user = user_list_head;
				while (find_user != NULL)
				{
					if (strcmp(find_user->username, username) == 0)
					{
						game->creator = find_user;
						break;
					}
					find_user = find_user->next;
				}

				sprintf(response, "GAME %s CREATED. WAITING FOR OPONENT TO JOIN.\r\n", game->gamename);
				sprintf(response, "%sa  %c | %c | %c \r\n", response, game->ttt[0][0], game->ttt[0][1], game->ttt[0][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sb  %c | %c | %c \r\n", response, game->ttt[1][0], game->ttt[1][1], game->ttt[1][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sc  %c | %c | %c \r\n", response, game->ttt[2][0], game->ttt[2][1], game->ttt[2][2]);
				sprintf(response, "%s\r\n", response);
				sprintf(response, "%s   %c   %c   %c\r\n", response, '1', '2', '3');

				game->state = 1; //challenger turn
				game->next = NULL;

				if (game_list_head == NULL)
				{
					game_list_head = game;
				}

				else
				{
					struct GAME *last = game_list_head;
					while (last->next != NULL)
					{
						last = last->next;
					}
					last->next = game;
				}
			}
			else if (logged == 0)
			{
				strcpy(response, "BAD PASSWORD");
			}
			else if (logged == -1)
			{
				strcpy(response, "USER NOT FOUND");
			}
		}
		else if (strcmp(command, "JOIN") == 0)
		{
			int logged = auth(username, password);
			if (logged == 1)
			{
				char *game_name = strtok(NULL, ",");
				char *position = strtok(NULL, ",");
				if (game_name == NULL || position == NULL)
				{
					sprintf(response, "JOIN COMMAND MUST BE CALLED AS FOLLOWS:\n");
					sprintf(response + strlen(response), "JOIN,USER,PASS,GAMENAME,SQUARE\n");
					write(client_sock, response, 2000);
					close(client_sock);
					continue;
				}

				//find the game
				struct GAME *find_game = game_list_head;
				while (find_game != NULL)
				{
					if (strcmp(find_game->gamename, game_name) == 0)
					{
						if (find_game->challenger != NULL)
						{
							strcpy(response, "GAME ALREADY HAS 2 PLAYERS!");
							goto end;
						}
						break;
					}
					find_game = find_game->next;
				}
				if (find_game == NULL)
				{
					sprintf(response, "GAME %s DOES NOT EXIST", game_name);
					goto end;
				}
				
				//find the user to link it to the game
				struct USER *find_user = user_list_head;
				while (find_user != NULL)
				{
					if (strcmp(find_user->username, username) == 0)
					{
						find_game->challenger = find_user;
						break;
					}
					find_user = find_user->next;
				}

				int taken = place_char(find_game, position, 'x');
				if (taken == 10)
				{
					strcpy(response, "INVALID MOVE. ROW MUST BE a-c");
					find_game->state = 1;
					goto end;
				}
				if (taken == 11)
				{
					strcpy(response, "INVALID MOVE. COL MUST BE 1-3");
					find_game->state = 1;
					goto end;
				}

				find_game->state = -1;

				sprintf(response, "GAME %s BETWEEN %s AND %s.\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username);
				sprintf(response, "%sIN PROGRESS: %s TO MOVE NEXT AS o\r\n", response, find_game->creator->username);
				sprintf(response, "%sa  %c | %c | %c \r\n", response, find_game->ttt[0][0], find_game->ttt[0][1], find_game->ttt[0][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sb  %c | %c | %c \r\n", response, find_game->ttt[1][0], find_game->ttt[1][1], find_game->ttt[1][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sc  %c | %c | %c \r\n", response, find_game->ttt[2][0], find_game->ttt[2][1], find_game->ttt[2][2]);
				sprintf(response, "%s\r\n", response);
				sprintf(response, "%s   %c   %c   %c\r\n", response, '1', '2', '3');
			}
			else if (logged == 0)
			{
				strcpy(response, "BAD PASSWORD");
			}
			else if (logged == -1)
			{
				strcpy(response, "USER NOT FOUND");
			}
		}
		else if (strcmp(command, "MOVE") == 0)
		{
			int logged = auth(username, password);
			if (logged = 1)
			{
				char *game_name = strtok(NULL, ",");
				char *position = strtok(NULL, ",");
				if (game_name == NULL || position == NULL)
				{
					sprintf(response, "MOVE COMMAND MUST BE CALLED AS FOLLOWS:\n");
					sprintf(response + strlen(response), "MOVE,USER,PASS,GAMENAME,SQUARE\n");
					write(client_sock, response, 2000);
					close(client_sock);
					continue;
				}

				struct GAME *find_game = game_list_head;
				while (find_game != NULL)
				{
					if (strcmp(find_game->gamename, game_name) == 0)
					{
						break;
					}
					find_game = find_game->next;
				}
				if (find_game == NULL)
				{
					sprintf(response, "GAME %s DOES NOT EXIST", game_name);
					goto end;
				}

				if (find_game->challenger == NULL)
				{
					sprintf(response, "GAME %s DOES NOT HAVE A CHALLENGER", find_game->gamename);
					goto end;
				}

				int taken;
				if (find_game->state == -1)
				{
					if (strcmp(find_game->creator->username, username) != 0)
					{
						strcpy(response, "IT IS NOT YOUR TURN!");
						goto end;
					}
					find_game->state = 1;
					taken = place_char(find_game, position, 'o');
				}
				else if (find_game->state == 1)
				{
					if (strcmp(find_game->challenger->username, username) != 0)
					{
						strcpy(response, "IT IS NOT YOUR TURN!");
						goto end;
					}
					taken = place_char(find_game, position, 'x');
					find_game->state = -1;
				}

				if (taken == 10)
				{
					strcpy(response, "INVALID MOVE. ROW MUST BE a-c");
					find_game->state = 1;
					goto end;
				}
				else if (taken == 11)
				{
					strcpy(response, "INVALID MOVE. COL MUST BE 1-3");
					find_game->state = 1;
					goto end;
				}
				else if (taken == -1)
				{
					strcpy(response, "INVALID MOVE, SQUARE NOT EMPTY");
					find_game->state = find_game->state * -1;
					goto end;
				}

				int winner = check_winner(find_game->ttt);

				switch (winner)
				{
				case -1:
					find_game->state = -2;
					sprintf(response, "GAME %s BETWEEN %s AND %s HAS ENDED.\nTHE WINNER IS %s\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username, find_game->creator->username);
					break;
				case 0:
					if (check_draw(find_game->ttt) == 1)
					{
						find_game->state = 0;
						sprintf(response, "GAME %s BETWEEN %s AND %s HAS ENDED.\nIT IS A DRAW\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username);
					}
					else
					{
						sprintf(response, "GAME %s BETWEEN %s AND %s.\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username);
						if (find_game->state == -1)
						{
							sprintf(response, "%sIN PROGRESS: %s TO MOVE NEXT AS o\r\n", response, find_game->creator->username);
						}
						else if (find_game->state == 1)
						{
							sprintf(response, "%sIN PROGRESS: %s TO MOVE NEXT AS x\r\n", response, find_game->challenger->username);
						}
					}
					break;
				case 1:
					find_game->state = 2;
					sprintf(response, "GAME %s BETWEEN %s AND %s HAS ENDED.\nTHE WINNER IS %s\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username, find_game->challenger->username);
					break;
				default:
					break;
				}
				sprintf(response, "%sa  %c | %c | %c \r\n", response, find_game->ttt[0][0], find_game->ttt[0][1], find_game->ttt[0][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sb  %c | %c | %c \r\n", response, find_game->ttt[1][0], find_game->ttt[1][1], find_game->ttt[1][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sc  %c | %c | %c \r\n", response, find_game->ttt[2][0], find_game->ttt[2][1], find_game->ttt[2][2]);
				sprintf(response, "%s\r\n", response);
				sprintf(response, "%s   %c   %c   %c\r\n", response, '1', '2', '3');
			}
			else if (logged == 0)
			{
				strcpy(response, "BAD PASSWORD");
			}
			else if (logged == -1)
			{
				strcpy(response, "USER NOT FOUND");
			}
		}
		else if (strcmp(command, "LIST") == 0)
		{
			sprintf(response, "LIST OF GAMES:\r\n");
			struct GAME *list_games = game_list_head;
			while (list_games != NULL)
			{
				if (list_games->state == -2)
				{
					sprintf(response, "%sGAME %s: CREATED BY %s, CHALLENGED BY: %s. %s IS THE WINNER\r\n", response, list_games->gamename, list_games->creator->username, list_games->challenger->username, list_games->creator->username);
				}
				else if (list_games->state == -1)
					sprintf(response, "%sGAME %s: CREATED BY %s, CHALLENGED BY: %s. IN PROGRESS: %s TO MOVE NEXT WITH o\r\n", response, list_games->gamename, list_games->creator->username, list_games->challenger->username, list_games->creator->username);
				else if (list_games->state == 0)
				{
					sprintf(response, "%sGAME %s: CREATED BY %s, CHALLENGED BY: %s. DRAW\r\n", response, list_games->gamename, list_games->creator->username, list_games->challenger->username);
				}
				else if (list_games->state == 1)
				{
					sprintf(response, "%sGAME %s: CREATED BY %s, CHALLENGED BY: %s. IN PROGRESS: %s TO MOVE NEXT WITH x\r\n", response, list_games->gamename, list_games->creator->username, list_games->challenger->username, list_games->challenger->username);
				}
				else if (list_games->state == 2)
				{
					sprintf(response, "%sGAME %s: CREATED BY %s, CHALLENGED BY: %s. %s IS THE WINNER\r\n", response, list_games->gamename, list_games->creator->username, list_games->challenger->username, list_games->challenger->username);
				}
				list_games = list_games->next;
			}
		}
		else if (strcmp(command, "SHOW") == 0)
		{
			int logged = auth(username, password);
			if (logged = 1)
			{
				char *game_name = strtok(NULL, ",");
				if (game_name == NULL)
				{
					sprintf(response, "SHOW COMMAND MUST BE CALLED AS FOLLOWS:\n");
					sprintf(response + strlen(response), "SHOW,USER,PASS,GAMENAME\n");
					write(client_sock, response, 2000);
					close(client_sock);
					continue;
				}

				struct GAME *find_game = game_list_head;
				while (find_game != NULL)
				{
					if (strcmp(find_game->gamename, game_name) == 0)
					{
						break;
					}
					find_game = find_game->next;
				}

				sprintf(response, "GAME %s BETWEEN %s AND %s.\r\n", find_game->gamename, find_game->creator->username, find_game->challenger->username);
				if (find_game->state == -1)
				{
					sprintf(response, "%sIN PROGRESS: %s TO MOVE NEXT AS o\r\n", response, find_game->creator->username);
				}
				else if (find_game->state == 1)
				{
					sprintf(response, "%sIN PROGRESS: %s TO MOVE NEXT AS x\r\n", response, find_game->challenger->username);
				}
				else if (find_game->state == 0)
				{
					sprintf(response, "%sGAME %s HAS ENDED.\nIT IS A DRAW\r\n", response, find_game->gamename);
				}
				else if (find_game->state == -2)
				{
					sprintf(response, "%sGAME %s HAS ENDED.\nTHE WINNER IS %s\r\n", response, find_game->gamename, find_game->creator->username);
				}
				else if (find_game->state == 2)
				{
					sprintf(response, "%sGAME %s HAS ENDED.\nTHE WINNER IS %s\r\n", response, find_game->gamename, find_game->challenger->username);
				}

				sprintf(response, "%sa  %c | %c | %c \r\n", response, find_game->ttt[0][0], find_game->ttt[0][1], find_game->ttt[0][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sb  %c | %c | %c \r\n", response, find_game->ttt[1][0], find_game->ttt[1][1], find_game->ttt[1][2]);
				sprintf(response, "%s  ---|---|---\r\n", response);
				sprintf(response, "%sc  %c | %c | %c \r\n", response, find_game->ttt[2][0], find_game->ttt[2][1], find_game->ttt[2][2]);
				sprintf(response, "%s\r\n", response);
				sprintf(response, "%s   %c   %c   %c\r\n", response, '1', '2', '3');
			}
			else if (logged == 0)
			{
				strcpy(response, "BAD PASSWORD");
			}
			else if (logged == -1)
			{
				strcpy(response, "USER NOT FOUND");
			}
		}
		else
		{
			sprintf(response, "COMMAND %s NOT IMPLEMENTED", command);
		}
	end:
		write(client_sock, response, 2000);
		close(client_sock);
	}

	close(socket_desc);

	return 0;
}
